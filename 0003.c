#include <stdio.h>

int main() {
    int r, c;
    scanf("%d%d", &r, &c);

    int matrix[r][c];
    int i, j, k;
    for(i = 0; i < r; i++) {
        for(j = 0; j < c; j++) {
            matrix[i][j] = 0;
        }
    }

    int tmp;
    for(i = 0; i < 2; i++) {
        for(j = 0; j < r; j++) {
            for(k = 0; k < c; k++) {
                scanf("%d", &tmp);
                matrix[j][k] += tmp;
            }
        }
    }

    for(i = 0; i < r; i++) {
        for(j = 0; j < c; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
    
    return 0;
}