#include <stdio.h>  // standard library to use

int main () {
    /* Variable declaration
     * n: a number of data
     * min: minimum data, starts with 2,000,000,000 (max) to ensure it will always work.
     * max: maximum data, starts with -2,000,000,000 (min) to ensure it will always work.
     * tmp: a temporary variable for each data of n
     */
    int n, min = 2000000000, max = -2000000000, tmp;

    scanf("%d", &n);    // read n

    // Compare each data.
    int i;  // iterator
    for (i = 0; i < n; i++) {
        scanf("%d", &tmp);  // read the current number

        // compare with the previous min, the first time will always work.
        if (tmp < min)
            min = tmp;
        
        // compare with the previous max, the first time will always work.
        if (tmp > max)
            max = tmp;
    }

    // Output
    printf("%d\n%d", min, max);

    return 0;
}
